import angular from 'angular';

// import common components
import { commonModule } from './components/common/common';

// import the required directives
import {
    headerModule,
    profileModule
} from './components/components';

angular.module('app', [
    commonModule.name,
    headerModule.name,
    profileModule.name
]);
