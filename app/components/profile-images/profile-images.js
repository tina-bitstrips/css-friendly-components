import angular from 'angular';
import { ProfileImagesComponent } from './profile-images.component';

let profileModule = angular.module('profileImages', [])
                        .directive('profileImages', ProfileImagesComponent);

export { profileModule };
