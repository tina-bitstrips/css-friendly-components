class ProfileImagesController {
    constructor(Sample) {
        this.Sample = Sample;
        this.templates = [];
        this.title = 'I am the Profile Images component';
    }

    getProfilePics() {
        this.Sample.getTemplates()
            .then( ({data}) => {
                data.forEach( (template) => {
                    this.templates.push(template);
                });
            });
        
        this.hideButton = true;
    }
}

ProfileImagesController.$inject = ['Sample'];

export { ProfileImagesController };
