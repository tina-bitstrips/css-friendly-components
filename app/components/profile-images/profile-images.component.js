import template from './profile-images.html';
import { ProfileImagesController as controller } from './profile-images.controller';

let ProfileImagesComponent = function () {
    return {
        restrict: 'E',
        scope: {},
        template,
        controllerAs: 'vm',
        controller
    };
};

export { ProfileImagesComponent };