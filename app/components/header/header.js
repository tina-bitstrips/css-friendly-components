import angular from 'angular';
import { HeaderComponent } from './header.component';

let headerModule = angular.module('header', []).directive('header', HeaderComponent);

export { headerModule };
