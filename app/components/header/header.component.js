import template from './header.html';
import { HeaderController as controller } from './header.controller';

let HeaderComponent = function () {
    return {
        restrict: 'E',
        scope: {},
        controllerAs: 'vm',
        template,
        controller
    };
};

export { HeaderComponent };
