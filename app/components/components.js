// all our components should be imported here
import { headerModule } from './header/header';
import { profileModule } from './profile-images/profile-images';

export {
    headerModule,
    profileModule
};