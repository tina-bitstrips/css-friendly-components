import angular from 'angular';
import { Sample } from './sample.factory';

let commonModule = angular.module('common', [])
    .factory('Sample', Sample);

export { commonModule };