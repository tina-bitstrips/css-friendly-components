let Sample = ($http) => {
    let getTemplates = () => {
        return $http.get('http://localhost:3000/templates');
    };

    return { getTemplates };
};

Sample.$inject = ['$http'];

export { Sample };
