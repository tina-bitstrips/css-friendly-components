var gulp = require('gulp');
var concat = require('gulp-concat');
var webpack = require('gulp-webpack');
var stylus = require('gulp-stylus');
var jeet = require('jeet');
var serve = require('browser-sync');
var sync = require('run-sequence');

var directories = {
    source: './app/',
    dest: './public/'
};

var paths = {
    html: directories.source + '**/*.html',
    css: directories.source + '**/*.styl',
    js: directories.source + '**/*.js',
    webpack: directories.source + '**/*.{js,html}'
};

/**
 * html
 */
gulp.task('indexhtml', function () {
    return gulp.src(directories.source + 'index.html').pipe(gulp.dest(directories.dest));
});


/**
 * css
 */
gulp.task('styles', function () {
    return gulp.src(paths.css)
        .pipe(stylus({
            use: [ jeet() ]
        }))
        .pipe(concat('app.css'))
        .pipe(gulp.dest(directories.dest + 'css/'))
});


/**
 * js
 */
gulp.task('webpack', function () {
    var config = {
        entry: directories.source + 'app.js',
        output: { filename: 'app.js' },
        devtool: 'source-map',
        module: {
            loaders: [
                { test: /\.js$/, loader: 'babel', exclude: /node_modules/ },
                { test: /\.html$/, loader: 'raw' }
            ]
        }
    };

    return gulp.src(paths.js)
        .pipe(webpack(config))
        .pipe(gulp.dest(directories.dest + 'js/'))
});


/**
 * browser sync server
 */
gulp.task('serve', function () {
    serve({
        port: 8008,
        open: false,
        server: {
            baseDir: directories.dest
        }
    });
});


/**
 * watch tasks
 */
gulp.task('watch', function () {
    gulp.watch([paths.html], [ 'indexhtml', serve.reload ]);
    gulp.watch([ paths.js, paths.html ], [ 'webpack', serve.reload ])
    gulp.watch([paths.css], [ 'styles', serve.reload ]);
});


/**
 * build
 */
gulp.task('build', [
    'styles',
    'indexhtml',
    'webpack'
]);


gulp.task('default', function (done) {
    // guarantees order
    sync('build', 'watch', 'serve', done);
});
