#  Modular JavaScript apps + CSS love

### `!important` Lunch & Learn about structuring modular apps, including CSS!

We will explore an alternative approach to building AngularJS apps (1.x):

- using ES6
- using controllerAs syntax (controllers as classes)
- directory structure that presents code in small, consumable components (easier dev onboarding)
- integrating CSS and **CSS preprocessing** into a modular, component-style workflow
  - avoid large CSS files
  - avoid fighting with CSS specificity, i.e. having to resort to `!important`

## Requirements

- Node.js (with local directory `node_modules` added to your path)

## Setup

```
npm install
```

Running the default Gulp task will process HTML, CSS, and JS using the chosen build tools:

```
gulp
```

## JSON server

Our Angular app needs to speak to a service. Run the server to get some sample data for the app:

```
json-server templates.json
```

This will serve the static `./templates.json` file on port 3000.


## Further reading

- More sample code – [this repo](https://github.com/angular-class/es6-workshop)
- controllerAs syntax – [this blog post](http://toddmotto.com/digging-into-angulars-controller-as-syntax/)
